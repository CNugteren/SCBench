
# ==================================================================================================
# This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
# project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
# width of 100 characters per line.
#
# Author(s):
#   Cedric Nugteren <cedric.nugteren@surfsara.nl>
#
# ==================================================================================================

# Colours
purplish  = "#550077" # [ 85,  0,119] lumi=26
blueish   = "#4765b1" # [ 71,101,177] lumi=100
redish    = "#d67568" # [214,117,104] lumi=136
greenish  = "#9bd4ca" # [155,212,202] lumi=199
colourset = c(blueish, greenish, redish, purplish)

# Maximum bandwidth displayed for bandwidth graphs. Everything higher than this is capped and the
# data is printed.
MAXBW = 50

# Minimum width of a graph: the higher this value, the smaller the individual bars
MINWIDTH = 10

# Set the width of the terminal
options("width"=170)

# ==================================================================================================

# Command-line arguments: no arguments for now
command_line <- commandArgs(trailingOnly=TRUE)
if (length(command_line) != 0) {
  print("Usage: Rscript graphs.r")
  quit()
}

# List of benchmarks
types <- c("0_device", "0_host", "0_mpi", "1_jacobi")
metrics <- c("bandwidth", "latency", "compute", "time")
ylabels <- c("bandwidth [GB/s]", "latency [us]", "compute [Gops/s]", "execution time [s]")

# ==================================================================================================

# Configures the outputfile
pdf("graphs.pdf", height=10, width=13)
par(mfrow=c(2, 2))
par(oma=c(0, 0, 0, 0))
par(mar=c(10, 4, 2, 0)) # bottom, left, top, right [c(5.1, 4.1, 4.1, 2.1)]
par(mgp=c(2.5, 0.6, 0)) # location of xlab/ylab, tick-mark labels, tick marks [c(3, 1, 0)]

# Loops over the benchmarks
mid <- 0
for (metric in c(metrics)) {
  mid <- mid + 1

  # Gathers all filenames
  filenames <- c()
  for (type in types) {
    filename <- paste(type, "_", metric, ".csv", sep="")
    if (file.exists(filename)) {
      filenames <- c(filenames, filename)
    }
  }
  if (length(filenames) == 0) {
    print(paste("No data found for metric '", metric, "', skipping...", sep=""))
    next
  }

  # Reads the CSV files into a dataframe
  readdata <- function(f) { read.csv(f, sep=";") }
  mergedata <- function(x,y) { merge(x, y, by="system", all=T) }
  datalist <- lapply(filenames, readdata)
  db <- Reduce(mergedata, datalist)

  # Sanitizes the dataframe
  rownames(db) <- db$system
  db$system <- NULL
  db[is.na(db)] <- 0
  print(db)

  # ================================================================================================

  # Plot settings
  spacing <- 1
  xmin <- 1
  xmax <- (nrow(db)+spacing)*ncol(db)
  xmax <- max(MINWIDTH, xmax)
  ymin <- 0
  ymax <- max(db)
  if (metric == "bandwidth") { ymax <- min(ymax, MAXBW) }

  # Creates an initial graph with axis but without data
  par(new=F)
  title <- paste(metric, " [4 MPI processes]", sep="")
  plot(x=xmin:xmax, y=rep(1, length(xmin:xmax)),
       main=title, xlab="", ylab=ylabels[mid],
       ylim=c(ymin, ymax), xlim=c(xmin, xmax), axes=F, "n")
  axis(side=2, las=2)
  grid()
  par(new=T)

  # Plots the data for this experiment
  mat <- as.matrix(db)
  barplot(mat, col=colourset[1:nrow(db)], space=c(0, spacing),
          main="", xlab="", ylab="", ylim=c(ymin, ymax), xlim=c(xmin, xmax),
          axes=F, xpd=F, add=T, beside=T, las=2,
          legend=rownames(db))

  # In case the data extends beyond the graph, adds a label
  for (r in seq(nrow(mat))) {
    for (c in seq(ncol(mat))) {
      if (mat[[r,c]] > ymax) {
        xpos <- (r+spacing)*c - 0.5
        text(xpos, MAXBW*1.1, round(mat[r,c]), xpd=T, srt=90)
      }
    }
  }
}

# ==================================================================================================
