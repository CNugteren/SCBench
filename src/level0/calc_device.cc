
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This benchmark measures the GPU's arithmetic throughput.
// For details, see the README file.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include "scbench.h"

#include <string>
#include <vector>
#include <algorithm>

namespace scbench {
// =================================================================================================

// See README file for a description
class SCCalcDevice: public SCBench {
 public:

  explicit SCCalcDevice(int argc, char* argv[]): SCBench(argc, argv) {

    // Only runs for processes with a CUDA device
    mpi_process_enabled_ = (device_id_ == args_.local_id);
    num_processes_ = static_cast<size_t>(mpi_.AllReduce(static_cast<int>(mpi_process_enabled_)));
  }

  // Describes a calculation-benchmark
  struct Calculation {
    std::string type;
    std::string op;
    std::string name;
    size_t modifier; // Set to 2 in case of fused multiply-add
  };

  // Benchmark code to test the communication
  virtual void Benchmark() override {

    // Number of operations per CUDA thread
    auto ops_per_thread = size_t{2048};

    // Minimum and maximum buffer sizes (in number of elements)
    args_.min = ops_per_thread*256*1024;
    args_.max = ops_per_thread*4096*1024;

    // Allocates memory on the host and device
    auto data_size = args_.max / ops_per_thread;
    std::vector<int> host_int32(data_size, 0);
    std::vector<float> host_fp32(data_size, 0);
    std::vector<double> host_fp64(data_size, 0);
    Buffer<int> dev_int32(context_, BufferAccess::kReadWrite, data_size);
    Buffer<float> dev_fp32(context_, BufferAccess::kReadWrite, data_size);
    Buffer<double> dev_fp64(context_, BufferAccess::kReadWrite, data_size);
    dev_int32.Write(queue_, data_size, host_int32);
    dev_fp32.Write(queue_, data_size, host_fp32);
    dev_fp64.Write(queue_, data_size, host_fp64);

    // Defines the benchmarks to be performed
    auto calculations = std::vector<Calculation>{
      {"int32", "add", "32-bit integer addition", 1},
      {"int32", "mul", "32-bit integer multiplication", 1},
      {"fp32", "mul", "32-bit floating point multiplication", 1},
      {"fp32", "fma", "32-bit floating point fused multiply-add", 2},
      {"fp64", "fma", "64-bit floating point fused multiply-add", 2},
      {"fp32", "__cosf", "32-bit floating point cosine fast-math", 1},
      //{"fp32", "cos", "32-bit floating point cosine IEEE-compliant", 1}
    };

    // Generates the device benchmark kernels as strings
    auto program_string = "#define OPS_PER_THREAD " + std::to_string(ops_per_thread) + "\n";
    for (auto &c: calculations) {

      // Sets the data-type
      auto tstring = std::string{};
      auto tasm = std::string{};
      auto treg = std::string{};
      if (c.type == "int32") { tstring = "int"; tasm = "u32"; treg = "r"; }
      if (c.type ==  "fp32") { tstring = "float"; tasm = "f32"; treg = "f"; }
      if (c.type ==  "fp64") { tstring = "double"; tasm = "f64"; treg = "d"; }

      // Sets the operation
      auto opstring = std::string{};
      if (c.op == "add" || c.op == "mul") {
        auto op = c.op;
        if (c.op == "mul" && c.type == "int32") { op = "mul.lo"; }
        opstring = "asm(\""+op+"."+tasm+" %0, %1, %2;\" : \"="+treg+"\"(t1) : \""+treg+"\"(t1), \""+treg+"\"(p2))";
      }
      if (c.op == "fma") {
        opstring = "asm(\"fma.rn."+tasm+" %0, %1, %2, %3;\" : \"="+treg+"\"(t1) : \""+treg+"\"(t1), \""+treg+"\"(t1), \""+treg+"\"(p2))";
      }
      if (c.op == "__cosf") {
        opstring = "asm(\"cos.approx."+tasm+" %0, %1;\" : \"="+treg+"\"(t1) : \""+treg+"\"(t1))";
      }
      if (c.op == "cos") {
        opstring = "t1 = cos(t1)";
      }

      // Generates the kernel
      program_string += "extern \"C\" __global__ void "+c.type+"_"+c.op;
      program_string += "("+tstring+"* mem, "+tstring+" p1, "+tstring+" p2) {\n";
      program_string += "  "+tstring+" t1 = p1;\n";
      for (auto i=size_t{0}; i<ops_per_thread; ++i) {
        program_string += "  "+opstring+";\n";
      }
      program_string += "  mem[threadIdx.x + blockDim.x*blockIdx.x] = t1;\n";
      program_string += "}\n";
    }

    // Creates a new program based on the kernel string. Then, builds this program and checks for
    // any compilation errors. If there are any, they are printed and execution is halted.
    auto program = Program(context_, program_string);
    auto compiler_options = std::vector<std::string>{};
    auto build_status = program.Build(device_, compiler_options);
    if (build_status != BuildStatus::kSuccess) {
      auto message = program.GetBuildInfo(device_);
      printf("* Compiler error(s)/warning(s) found:\n%s\n", message.c_str());
      return;
    }

    // Optionally also print the resulting IR (e.g. PTX) to inspect the kernel
    //printf("%s\n", program_string.c_str());
    //printf("\n* IR:\n%s\n", program.GetIR().c_str());

    // Sets the arguments to some value
    auto i1 = 1; auto f1 = 1.0f; auto d1 = 1.0;
    auto i2 = 2; auto f2 = 2.0f; auto d2 = 2.0;

    // Runs the benchmarks
    for (auto &c: calculations) {
      StartBenchmark(c.type+" ["+c.op+"]", c.name);
      auto kernel = Kernel(program, c.type+"_"+c.op);
      if (c.type == "int32") { kernel.SetArguments(dev_int32, i1, i2); }
      if (c.type ==  "fp32") { kernel.SetArguments(dev_fp32, f1, f2); }
      if (c.type ==  "fp64") { kernel.SetArguments(dev_fp64, d1, d2); }
      auto lambda = [&kernel, &ops_per_thread, this](const size_t size) {
        auto global = std::vector<size_t>{size / ops_per_thread};
        auto local = std::vector<size_t>{256};
        kernel.Launch(queue_, global, local, event_);
        queue_.Finish(event_);

      };
      TestCalculation(lambda, num_processes_*c.modifier, c.type+"_"+c.op);
    }
  }
};

// =================================================================================================
} // namespace scbench

// Main function (not within the scbench namespace)
int main(int argc, char *argv[]) {
  scbench::SCCalcDevice benchmark(argc, argv);
  benchmark.Run("device calculations");
  benchmark.PrintTable("device");
  return 0;
}

// =================================================================================================
