
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This benchmark measures the CPU's arithmetic throughput.
// For details, see the README file.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include "scbench.h"

#include <string>
#include <vector>
#include <algorithm>
#include <valarray>

namespace scbench {
// =================================================================================================

// See README file for a description
class SCCalcHost: public SCBench {
 public:

  explicit SCCalcHost(int argc, char* argv[]): SCBench(argc, argv) {}

  // Describes a calculation-benchmark
  struct Calculation {
    std::string type;
    std::string op;
    std::string name;
    size_t modifier; // Set to 2 in case of fused multiply-add
  };

  // Benchmark code to test the communication
  virtual void Benchmark() override {

    // Vector width, implemented using std::valarray
    constexpr auto kVector = size_t{4};

    // Number of operations in the (unrolled?) inner-loop
    constexpr auto kOpsPerIter = size_t{512};

    // Minimum and maximum buffer sizes (in number of elements)
    args_.min = kOpsPerIter*8*1024;
    args_.max = kOpsPerIter*64*1024;

    // Defines all benchmarks to be performed
    auto calculations = std::vector<Calculation>{
      {"int32", "mul", "32-bit integer multiplication", 1},
      {"fp32", "mul", "32-bit floating point multiplication", 1},
      {"fp32", "fma", "32-bit fused multiply-add", 2},
    };

    // Vector data-types
    using vint = std::valarray<int>;
    using vfloat = std::valarray<float>;

    // Loops over all benchmarks
    for (auto &c: calculations) {
      auto lambda = [&c, this](const size_t size) {

        // int32_mul
        if (c.type == "int32" && c.op == "mul") {
          auto operation = []() -> vint {
            vint p0(kVector);
            vint p1(kVector);
            for (auto i=size_t{0}; i<kOpsPerIter; ++i) { p0 *= p1; }
            return p0;
          };
          for (auto s=size_t{0}; s<size/kOpsPerIter; ++s) {
            volatile auto result = operation();
          }
        }

        // fp32_mul
        if (c.type == "fp32" && c.op == "mul") {
          auto operation = []() -> vfloat {
            vfloat p0(kVector);
            vfloat p1(kVector);
            for (auto i=size_t{0}; i<kOpsPerIter; ++i) { p0 *= p1; }
            return p0;
          };
          for (auto s=size_t{0}; s<size/kOpsPerIter; ++s) {
            volatile auto result = operation();
          }
        }

        // fp32_fma
        if (c.type == "fp32" && c.op == "fma") {
          auto operation = []() -> vfloat {
            vfloat p0(kVector);
            vfloat p1(kVector);
            for (auto i=size_t{0}; i<kOpsPerIter; ++i) { p0 += p0 * p1; }
            return p0;
          };
          for (auto s=size_t{0}; s<size/kOpsPerIter; ++s) {
            volatile auto result = operation();
          }
        }
      };

      // Adds the benchmark
      StartBenchmark(c.type+" ["+c.op+"]", c.name);
      TestCalculation(lambda, num_processes_ * c.modifier * kVector, c.type+"_"+c.op);
    }
  }
};

// =================================================================================================
} // namespace scbench

// Main function (not within the scbench namespace)
int main(int argc, char *argv[]) {
  scbench::SCCalcHost benchmark(argc, argv);
  benchmark.Run("host calculations");
  benchmark.PrintTable("host");
  return 0;
}

// =================================================================================================
