
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This benchmark measures the GPU memory bandwidth and latency.
// For details, see the README file.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include "scbench.h"

#include <string>
#include <vector>
#include <algorithm>

namespace scbench {
// =================================================================================================

// See README file for a description
template <typename T>
class SCCommDevice: public SCBench {
 public:

  explicit SCCommDevice(int argc, char* argv[]): SCBench(argc, argv) {

    // Only runs for processes with a CUDA device
    mpi_process_enabled_ = (device_id_ == args_.local_id);
    num_processes_ = static_cast<size_t>(mpi_.AllReduce(static_cast<int>(mpi_process_enabled_)));
  }

  // Benchmark code to test the communication
  virtual void Benchmark() override {

    // Minimum and maximum buffer sizes (in number of elements)
    args_.min = 32*1024;
    args_.max = 4*1024*1024;

    // Allocates memory on the host (pageable and pinned)
    std::vector<T> host_x(args_.max, 0);
    std::vector<T> host_y(args_.max, 0);
    BufferHost<T> host_p(context_, args_.max);
    BufferHost<T> host_q(context_, args_.max);

    // Allocates memory on the device and fills it with initial contents
    Buffer<T> dev_x(context_, BufferAccess::kReadWrite, args_.max);
    Buffer<T> dev_y(context_, BufferAccess::kReadWrite, args_.max);
    dev_x.Write(queue_, args_.max, host_x);
    dev_y.Write(queue_, args_.max, host_y);

    // Host to device
    StartBenchmark("H->D [PAGEABLE]", "Host-to-device pageable copy");
    auto host_to_device = [&dev_x, &host_y, this](const size_t size) {
      dev_x.Write(queue_, size, host_y);
    };
    TestLatency(host_to_device, "HtoD_pageable");
    TestBandwidth(host_to_device, num_processes_*sizeof(T), "HtoD_pageable"); // *1 for bus
    StartBenchmark("H->D [PINNED]", "Host-to-device pinned copy");
    auto host_to_device_pinned = [&dev_x, &host_q, this](const size_t size) {
      dev_x.Write(queue_, size, host_q);
    };
    TestLatency(host_to_device_pinned, "HtoD_pinned");
    TestBandwidth(host_to_device_pinned, num_processes_*sizeof(T), "HtoD_pinned"); // *1 for bus

    // Device to host
    StartBenchmark("D->H [PAGEABLE]", "Device-to-host pageable copy");
    auto device_to_host = [&dev_x, &host_y, this](const size_t size) {
      dev_x.Read(queue_, size, host_y);
    };
    TestLatency(device_to_host, "DtoH_pageable");
    TestBandwidth(device_to_host, num_processes_*sizeof(T), "DtoH_pageable"); // *1 for bus
    StartBenchmark("D->H [PINNED]", "Device-to-host pinned copy");
    auto device_to_host_pinned = [&dev_x, &host_q, this](const size_t size) {
      dev_x.Read(queue_, size, host_q);
    };
    TestLatency(device_to_host_pinned, "DtoH_pinned");
    TestBandwidth(device_to_host_pinned, num_processes_*sizeof(T), "DtoH_pinned"); // *1 for bus

    // Device to device
    StartBenchmark("D->D", "Device-to-device copy");
    auto device_to_device = [&dev_x, &dev_y, this](const size_t size) {
      dev_x.CopyTo(queue_, size, dev_y);
    };
    TestLatency(device_to_device, "DtoD");
    TestBandwidth(device_to_device, num_processes_*sizeof(T)*2, "DtoD"); // 2* for read+write
  }
};

// =================================================================================================
} // namespace scbench

// Main function (not within the scbench namespace)
int main(int argc, char *argv[]) {
  scbench::SCCommDevice<double> benchmark(argc, argv);
  benchmark.Run("device communication");
  benchmark.PrintTable("device");
  return 0;
}

// =================================================================================================
