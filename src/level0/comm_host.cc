
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This benchmark measures the CPU memory bandwidth and latency.
// For details, see the README file.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include "scbench.h"

#include <cstring>
#include <string>
#include <vector>
#include <algorithm>

namespace scbench {
// =================================================================================================

// See README file for a description
template <typename T>
class SCCommHost: public SCBench {
 public:

  explicit SCCommHost(int argc, char* argv[]): SCBench(argc, argv) {}

  // Benchmark code to test the communication
  virtual void Benchmark() override {

    // Minimum and maximum buffer sizes (in number of elements)
    args_.min = 1*1024*1024;
    args_.max = 8*1024*1024;

    // Allocates memory on the host (std::vector and CUDA pinned memory)
    std::vector<T> host_x(args_.max, 0);
    std::vector<T> host_y(args_.max, 0);
    BufferHost<T> host_p(context_, args_.max);
    BufferHost<T> host_q(context_, args_.max);

    // Adds a benchmark
    StartBenchmark("H->H [COPY_N]", "Host memory copy (std::copy_n)");
    auto vector = [&host_x, &host_y](const size_t size) {
      std::copy_n(host_y.begin(), size, std::back_inserter(host_x));
    };
    TestLatency(vector, "HtoH_copy_n");
    TestBandwidth(vector, num_processes_*sizeof(T)*2, "HtoH_copy_n"); // *2 for read+write

    // Adds a benchmark
    StartBenchmark("H->H [MEMCPY]", "Host memory copy (memcpy)");
    auto vector_memcpy = [&host_x, &host_y](const size_t size) {
      memcpy(host_x.data(), host_y.data(), size*sizeof(T));
    };
    TestLatency(vector_memcpy, "HtoH_memcpy");
    TestBandwidth(vector_memcpy, num_processes_*sizeof(T)*2, "HtoH_memcpy"); // *2 for read+write

    // Adds a benchmark
    StartBenchmark("H->H [PINNED]", "Host memory copy (cudaMallocHost and memcpy)");
    auto cuda_pinned = [&host_p, &host_q](const size_t size) {
      memcpy(host_q.data(), host_p.data(), size*sizeof(T));
    };
    TestLatency(cuda_pinned, "HtoH_pinned");
    TestBandwidth(cuda_pinned, num_processes_*sizeof(T)*2, "HtoH_pinned"); // *2 for read+write
  }
};

// =================================================================================================
} // namespace scbench

// Main function (not within the scbench namespace)
int main(int argc, char *argv[]) {
  scbench::SCCommHost<double> benchmark(argc, argv);
  benchmark.Run("host memory");
  benchmark.PrintTable("host");
  return 0;
}

// =================================================================================================
