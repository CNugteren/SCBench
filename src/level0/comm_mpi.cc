
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This benchmark measures the MPI communication bandwidth and latency.
// For details, see the README file.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include "scbench.h"

#include <string>
#include <vector>
#include <algorithm>

namespace scbench {
// =================================================================================================

// See README file for a description
template <typename T>
class SCCommMPI: public SCBench {
 public:

  explicit SCCommMPI(int argc, char* argv[]): SCBench(argc, argv) {

    // Runs with at most 2 nodes and 2 processes per node
    mpi_process_enabled_ = (args_.local_id < 2 && args_.node_id < 2);
    num_processes_ = static_cast<size_t>(mpi_.AllReduce(static_cast<int>(mpi_process_enabled_)));
  }

  // Benchmark code to test the communication
  virtual void Benchmark() override {

    // Minimum and maximum buffer sizes (in number of elements)
    args_.min = 32*1024;
    args_.max = 4*1024*1024;

    // Allocates memory on the host and device
    std::vector<T> host_x(args_.max, 0);
    std::vector<T> host_y(args_.max, 0);
    Buffer<T> dev_x(context_, BufferAccess::kReadWrite, args_.max);
    Buffer<T> dev_y(context_, BufferAccess::kReadWrite, args_.max);
    dev_x.Write(queue_, args_.max, host_x);
    dev_y.Write(queue_, args_.max, host_y);

    // Only perform the MPI intra-node benchmarks with two local ranks
    if (args_.num_local >= 2) {
      mpi_process_enabled_ = (args_.local_id < 2 && args_.node_id == 0);
      num_processes_ = static_cast<size_t>(mpi_.AllReduce(static_cast<int>(mpi_process_enabled_)));
      auto comm_local = mpi_.CommLocal();

      // Copies from host memory host_y to host memory host_x of the next rank (within a node)
      StartBenchmark("H->H [intra]", "MPI intra-node host-to-host communication");
      auto intra_host = [&host_x, &host_y, &comm_local, this](const size_t size) {
        auto tag = 1;
        if (args_.local_id == 0) { mpi_.Send(size, 1, tag, host_y.data(), comm_local); }
        if (args_.local_id == 1) { mpi_.Recv(size, 0, tag, host_x.data(), comm_local); }
      };
      TestLatency(intra_host, "HtoH_intra");
      TestBandwidth(intra_host, sizeof(T), "HtoH_intra");

      // As above, but for device memory (from dev_y via host_y and host_x to dev_x)
      StartBenchmark("D->D [intra]", "MPI intra-node device-to-device communication");
      auto intra_device = [&host_x, &host_y, &dev_x, &dev_y, &comm_local, this](const size_t size) {
        auto tag = 1;
        if (args_.local_id == 0) { dev_y.Read(queue_, size, host_y); }
        if (args_.local_id == 0) { mpi_.Send(size, 1, tag, host_y.data(), comm_local); }
        if (args_.local_id == 1) { mpi_.Recv(size, 0, tag, host_x.data(), comm_local); }
        if (args_.local_id == 1) { dev_x.Write(queue_, size, host_x); }
      };
      TestLatency(intra_device, "DtoD_intra");
      TestBandwidth(intra_device, sizeof(T), "DtoD_intra");

      // As above, but with GPU-Direct RDMA
      if (kUseGPUDirect) {
        StartBenchmark("D->D [intra, GPU-Direct]", "MPI intra-node device-to-device communication with GPU-Direct");
        auto intra_device_rdma = [&host_x, &host_y, &dev_x, &dev_y, &comm_local, this](const size_t size) {
          auto tag = 1;
          if (args_.local_id == 0) { mpi_.Send(size, 1, tag, reinterpret_cast<T*>(dev_y()), comm_local); }
          if (args_.local_id == 1) { mpi_.Recv(size, 0, tag, reinterpret_cast<T*>(dev_x()), comm_local); }
        };
        TestLatency(intra_device_rdma, "DtoD_intra_gdr");
        TestBandwidth(intra_device_rdma, sizeof(T), "DtoD_intra_gdr");
      }
      else {
        if (args_.rank == 0) {
          printf("\n* Skipping GPU-Direct benchmark: disabled by user\n");
        }
      }
    }
    else {
      if (args_.rank == 0) {
        printf("\n* Skipping intra-node benchmarks: requires 2 MPI processes per node but found %lu\n",
               args_.num_local);
      }
    }

    // Only perform the MPI inter-node benchmarks with two nodes, one rank per node
    if (args_.num_nodes >= 2) {
      mpi_process_enabled_ = (args_.local_id == 0 && args_.node_id < 2);
      num_processes_ = static_cast<size_t>(mpi_.AllReduce(static_cast<int>(mpi_process_enabled_)));
      auto comm_world = mpi_.CommWorld();

      // Sets the ranks to consider
      auto rank_0 = size_t{0};
      auto rank_1 = args_.num_local; // number of devices within a node

      // Copies from host memory host_y to host memory host_x to the next node
      StartBenchmark("H->H [inter]", "MPI inter-node host-to-host communication");
      auto inter_host = [&rank_0, &rank_1, &host_x, &host_y, &comm_world, this](const size_t size) {
        auto tag = 1;
        if (args_.rank == rank_0) { mpi_.Send(size, rank_1, tag, host_y.data(), comm_world); }
        if (args_.rank == rank_1) { mpi_.Recv(size, rank_0, tag, host_x.data(), comm_world); }
      };
      TestLatency(inter_host, "HtoH_inter");
      TestBandwidth(inter_host, sizeof(T), "HtoH_inter");

      // As above, but for device memory (from dev_y via host_y and host_x to dev_x)
      StartBenchmark("D->D [inter]", "MPI inter-node device-to-device communication");
      auto inter_device = [&rank_0, &rank_1, &host_x, &host_y, &dev_x, &dev_y, &comm_world, this](const size_t size) {
        auto tag = 1;
        if (args_.rank == rank_0) { dev_y.Read(queue_, size, host_y); }
        if (args_.rank == rank_0) { mpi_.Send(size, rank_1, tag, host_y.data(), comm_world); }
        if (args_.rank == rank_1) { mpi_.Recv(size, rank_0, tag, host_x.data(), comm_world); }
        if (args_.rank == rank_1) { dev_x.Write(queue_, size, host_x); }
      };
      TestLatency(inter_device, "DtoD_inter");
      TestBandwidth(inter_device, sizeof(T), "DtoD_inter");

      // As above, but with GPU-Direct RDMA
      if (kUseGPUDirect) {
        StartBenchmark("D->D [inter, GPU-Direct]", "MPI inter-node device-to-device communication with GPU-Direct");
        auto inter_device_rdma = [&rank_0, &rank_1, &host_x, &host_y, &dev_x, &dev_y, &comm_world, this](const size_t size) {
          auto tag = 1;
          if (args_.rank == rank_0) { mpi_.Send(size, rank_1, tag, reinterpret_cast<T*>(dev_y()), comm_world); }
          if (args_.rank == rank_1) { mpi_.Recv(size, rank_0, tag, reinterpret_cast<T*>(dev_x()), comm_world); }
        };
        TestLatency(inter_device_rdma, "DtoD_inter_gdr");
        TestBandwidth(inter_device_rdma, sizeof(T), "DtoD_inter_gdr");
      }
      else {
        if (args_.rank == 0) {
          printf("\n* Skipping GPU-Direct benchmark: disabled by user\n");
        }
      }
    }
    else {
      if (args_.rank == 0) {
        printf("\n* Skipping inter-node benchmarks: requires 2 MPI nodes but found %lu\n",
               args_.num_nodes);
      }
    }
  }
};

// =================================================================================================
} // namespace scbench

// Main function (not within the scbench namespace)
int main(int argc, char *argv[]) {
  scbench::SCCommMPI<double> benchmark(argc, argv);
  benchmark.Run("mpi communication");
  benchmark.PrintTable("mpi");
  return 0;
}

// =================================================================================================
