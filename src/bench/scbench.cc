
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This file provides the implementation of the base-class for the SCBench benchmarks. See the
// header file for a description.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include "scbench.h"

#include <string>
#include <vector>
#include <chrono>
#include <limits>
#include <algorithm>
#include <iostream>
#include <thread>

namespace scbench {
// =================================================================================================

// Constructor for the base benchmark class. This initializes MPI and CUDA.
SCBench::SCBench(int argc, char *argv[]):
  mpi_(),
  mpi_process_enabled_(true),
  num_processes_(mpi_.GetSize()),
  args_(GetArguments(argc, argv)),
  platform_(Platform(0)),
  device_id_(args_.local_id % platform_.NumDevices()),
  device_(Device(platform_, device_id_)),
  context_(Context(device_)),
  queue_(Queue(context_, device_)),
  event_(Event()) {
}

// Runs the benchmark: this function prints a header and the calls a virtual method to be
// implemented by the derived class
void SCBench::Run(const std::string &name) {

  // Prints the header
  if (args_.rank == 0) {
    printf("\n* Start of benchmark '%s%s%s' with MPI processes:\n",
           kPrintPink.c_str(), name.c_str(), kPrintEnd.c_str());
    fflush(stdout);
  }
  mpi_.Barrier();
  if (mpi_process_enabled_) {
    printf("    Rank %lu, node %lu, rank %lu within node, device %lu: '%s'\n",
           args_.rank, args_.node_id, args_.local_id, device_id_, device_.Name().c_str());
    fflush(stdout);
  }
  mpi_.Barrier();
  std::this_thread::sleep_for(std::chrono::milliseconds(10));

  // Runs the benchmark
  Benchmark();
  if (args_.rank == 0) { printf("\n"); }
}

// Prints a summary table of results, both to screen and to file. If the file already exists, a new
// line is appended.
void SCBench::PrintTable(const std::string &name) const {
  if (args_.rank == 0) {

    // Prints results to screen
    printf("* CSV table with best-case results:\n %18s", " ");
    if (results_calculation_.size() > 1) {
      for (auto &result: results_calculation_) { printf(" | %18s", result.first.c_str()); }
      printf("\n");
      for (auto j=size_t{0}; j<=results_calculation_.size(); ++j) { for (auto i=0; i<20; ++i) { printf("-"); } printf("x"); }
      printf("\n %18s", "compute [Gops/s]");
      for (auto &result: results_calculation_) { printf(" | %18.1lf", result.second); }
    }
    else {
      for (auto &result: results_bandwidth_) { printf(" | %18s", result.first.c_str()); }
      printf("\n");
      for (auto j=size_t{0}; j<=results_bandwidth_.size(); ++j) { for (auto i=0; i<20; ++i) { printf("-"); } printf("x"); }
      printf("\n %18s", "bandwidth [GB/s]");
      for (auto &result: results_bandwidth_) { printf(" | %18.1lf", result.second); }
      printf("\n %18s", "latency [us]");
      for (auto &result: results_latency_) { printf(" | %18.3lf", result.second); }
    }
    printf("\n\n");

    // Describes how to print results to a file
    auto TableToFile = [&name, this](const std::string &type, const Collection &results) {
      if (results.size() > 1) {
        auto system_name = device_.Name();
        auto filename = "0_" + name + "_" + type + ".csv";

        // Only writes the header if the file did not yet exist: first check whether it exists
        auto new_file = true;
        if (auto f = fopen(filename.c_str(), "r")) { new_file = false; fclose(f); }
        auto file = fopen(filename.c_str(), "a");
        if (new_file) {
          fprintf(file, "system");
          for (auto &result: results) {
            fprintf(file, ";%s", result.first.c_str());
          }
          fprintf(file, "\n");
        }

        // Writes the entry
        fprintf(file, "%s", system_name.c_str());
        for (auto &result: results) {
          fprintf(file, ";%.3lf", result.second);
        }
        fprintf(file, "\n");
        fclose(file);
      }
    };

    // Prints results to file
    TableToFile("compute", results_calculation_);
    TableToFile("bandwidth", results_bandwidth_);
    TableToFile("latency", results_latency_);
  }
}

// =================================================================================================

// Retrieves arguments from the command-line. Currently, command-line arguments are not used. All
// arguments are set based on MPI information.
SCBench::Arguments SCBench::GetArguments(int argc, char *argv[]) {
  Arguments args{};
  args.num_ranks = mpi_.GetSize();
  args.rank = mpi_.GetRank();
  args.num_nodes = mpi_.GetNodeCount();
  args.node_id = mpi_.GetNodeRank();
  args.num_local = mpi_.GetLocalCount();
  args.local_id = mpi_.GetLocalRank();
  return args;
}

// =================================================================================================

// Takes a function as an argument and executes it while measuring execution time. This includes
// two MPI barries: to make sure the measured time is the time of the slowest MPI process. The
// timing result will thus be more or less the same on each rank. Note that this is repeat a given
// number of times to get proper timing statistics: both the outer loop as well as the inner loop
// are repeated.
Timings SCBench::TimedExecution(const TimedFunction &timed_function, const size_t size,
                                const size_t repetitions_inner) {
  auto timings = Timings(kRepetitionsOuter);
  for (auto t=size_t{0}; t<kRepetitionsOuter; ++t) {

    // Starts the timer
    mpi_.Barrier();
    auto start_time = std::chrono::steady_clock::now();

    // Runs the timed function multiple times
    if (mpi_process_enabled_) {
      for (auto u=size_t{0}; u<repetitions_inner; ++u) {
        timed_function(size);
      }
    }

    // Ends the timer
    mpi_.Barrier();
    auto elapsed_time = std::chrono::steady_clock::now() - start_time;
    auto timing = std::chrono::duration<double,std::milli>(elapsed_time).count();
    timings.Add(t, timing/static_cast<float>(repetitions_inner));
  }
  return timings;
}

// =================================================================================================

// Prints the header for a new benchmark
void SCBench::StartBenchmark(const std::string &summary, const std::string &title) {
  if (args_.rank == 0) {
    printf("\n");
    printf("* %s%s%s: %s\n", kPrintGreen.c_str(), summary.c_str(), kPrintEnd.c_str(), title.c_str());
    fflush(stdout);
  }
}

// =================================================================================================

// Tests a given benchmark function in a loop over a size argument, ranging from a user-defined
// minimum to a maximum in powers of 2. This tests memory bandwidth.
void SCBench::TestBandwidth(const TimedFunction timed_function, const size_t modifier,
                            const std::string &id) {
  auto best_result = std::numeric_limits<float>::min();
  for (auto s=args_.min; s<=args_.max; s=s*2) {
    auto time_ms = TimedExecution(timed_function, s, kRepetitionsInnerBandwidth);
    auto bytes = modifier*s;
    auto result = PrintBandwidth(time_ms, bytes);
    best_result = std::max(best_result, result);
  }
  results_bandwidth_.emplace(id, best_result);
}

// As above, but now tests communication latency
void SCBench::TestLatency(const TimedFunction timed_function, const std::string &id) {
  auto time_ms = TimedExecution(timed_function, 1, kRepetitionsInnerLatency);
  auto result = PrintLatency(time_ms);
  results_latency_.emplace(id, result);
}

// As above, but now tests calculation throughput (ALU/SFU)
void SCBench::TestCalculation(const TimedFunction timed_function, const size_t modifier,
                              const std::string &id) {
  auto best_result = std::numeric_limits<float>::min();
  for (auto s=args_.min; s<=args_.max; s=s*2) {
    auto time_ms = TimedExecution(timed_function, s, kRepetitionsInnerCalculation);
    auto ops = modifier*s;
    auto result = PrintCalculation(time_ms, ops);
    best_result = std::max(best_result, result);
  }
  results_calculation_.emplace(id, best_result);
}

// =================================================================================================

// Reports the timing results to screen and returns the value of the printed metric. This reports
// memory bandwidth.
float SCBench::PrintBandwidth(const Timings &timings, const size_t bytes) {
  auto best_time = timings.GetBest();
  auto gigabytes = bytes / (1024.0*1024.0*1024.0);
  auto megabytes = bytes / (1024.0*1024.0);
  auto result = gigabytes*1.0e3/best_time;
  if (args_.rank == 0) {
    printf("   %8.1lf MB in %7.3lf ms: %7.1lf GB/s    %s\n",
           megabytes, best_time, result, timings.GetDistribution().c_str());
    fflush(stdout);
  }
  return result;
}

// As above, but now for the communication latency
float SCBench::PrintLatency(const Timings &timings) {
  auto best_time = timings.GetBest();
  auto result = best_time*1.0e3;
  if (args_.rank == 0) {
    printf("       latency of %7.3lf us                  %s\n",
           result, timings.GetDistribution().c_str());
    fflush(stdout);
  }
  return result;
}

// As above, but now for the calculation throughput (ALU/SFU)
float SCBench::PrintCalculation(const Timings &timings, const size_t ops) {
  auto best_time = timings.GetBest();
  auto gigaops = ops / (1024.0*1024.0*1024.0);
  auto megaops = ops / (1024.0*1024.0);
  auto result = gigaops*1.0e3/best_time;
  if (args_.rank == 0) {
    printf("   %8.1lf Mops in %7.3lf ms: %7.1lf Gops/s    %s\n",
           megaops, best_time, result, timings.GetDistribution().c_str());
    fflush(stdout);
  }
  return result;
}

// =================================================================================================
} // namespace scbench
