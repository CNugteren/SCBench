
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This file replaces:
// - The command-line argument parser with fixed settings (input.cc no longer needed).
// - The performance printer with one that writes to file (replaces PostRunJacobi in host.cc)
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#include <string>
#include "source/jacobi.h"

// =================================================================================================

// The original command-line parser is replaced by one with fixed values
int ParseCommandLineArguments(int, char **, int rank, int num_ranks,
                              int2* local, int2* topology, int* use_fast_swap) {

  // Sets the global domain to a constant
  int global_x = 8192;
  int global_y = 8192;

  // Computes the sizes of the (rectangular) local domains
  topology->x = num_ranks;
  topology->y = 1;
  local->x = global_x / topology->x;
  local->y = global_y / topology->y;

  // Uses fast swapping
  *use_fast_swap = 1;

  // Output the settings to screen
  if (rank == MPI_MASTER_RANK) {
    printf("Topology size: %d x %d\n", topology->x, topology->y);
    printf("Local domain size (current node): %d x %d\n", local->x, local->y);
    printf("Global domain size (all nodes): %d x %d\n", global_x, global_y);
  }
  return STATUS_OK;
}

// =================================================================================================

// Forward declarations for the PostRunJacobi function
void UpdatePerfCounters(const int2*, const int2*, int, int, double*, double*, double*);
void PrintPerfCounter(const char*, const char*, double, double, int);

// The function to write to screen is replaced: this one also writes to disk
void PostRunJacobi(MPI_Comm communicator, int rank, int num_ranks,
                   const int2* topology, const int2* local, int iterations, int use_fast_swap,
                   double start_time, double transfer_time) {
  double elapsed_time;
  double latt_updates = 0.0, flops = 0.0, bandwidth = 0.0;
  MPI_Barrier(communicator);
  elapsed_time = MPI_Wtime() - start_time;
  
  // Prints the performance counters to screen
  if (rank == MPI_MASTER_RANK) {
    printf("Total Jacobi run time: %.4lf sec.\n", elapsed_time);
    printf("Average per-process communication time: %.4lf sec.\n", transfer_time);

    // Computes the performance counters over all MPI processes
    UpdatePerfCounters(topology, local, iterations, use_fast_swap, &latt_updates, &flops, &bandwidth);
  
    PrintPerfCounter("Measured lattice updates", "LU/s", latt_updates, elapsed_time, num_ranks);
    PrintPerfCounter("Measured FLOPS", "FLOPS", flops, elapsed_time, num_ranks);
    PrintPerfCounter("Measured device bandwidth", "B/s", bandwidth, elapsed_time, num_ranks);
  }

  // Prints the elapsed time to disk
  if (rank == MPI_MASTER_RANK) {
    std::string filename = "1_jacobi_time.csv";

    // Retrieves the 'system name': the name of the GPU
    struct cudaDeviceProp properties;
    int device = 0;
    SafeCudaCall(cudaGetDevice(&device));
    SafeCudaCall(cudaGetDeviceProperties(&properties, device));
    std::string system_name = properties.name;

    // Only writes the header if the file did not yet exist: first check whether it exists
    bool new_file = true;
    if (FILE* f = fopen(filename.c_str(), "r")) { new_file = false; fclose(f); }
    FILE* file = fopen(filename.c_str(), "a");
    if (new_file) {
      fprintf(file, "system;jacobi\n");
    }

    // Writes the entry
    fprintf(file, "%s;%.3lf\n", system_name.c_str(), elapsed_time);
    fclose(file);
  }
}

// =================================================================================================
