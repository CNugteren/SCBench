
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This file implements a simple MPI C++11 API. This contains a single class which automatically
// constructs and destroys the MPI environment. All MPI API calls in this file are automatically
// checked for errors.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#ifndef SCBENCH_MPIPP11_H_
#define SCBENCH_MPIPP11_H_

#include "mpi.h"

#include <string> // std::string
#include <vector> // std::vector
#include <stdexcept> // std::runtime_error

// This MPI library supports the MPI 3 standard - or not?
#define HAVE_MPI_3 true

namespace scbench {
// =================================================================================================

// See comment at top of file for a description of the class
class MPI {
 public:

  // Memory management
  explicit MPI() {
    auto status = MPI_Init(nullptr, nullptr);
    if (status != MPI_SUCCESS) { Error(status); }
  }
  ~MPI() {
    auto status = MPI_Finalize();
    if (status != MPI_SUCCESS) { Error(status); }
  }
  MPI(const MPI &other) = delete;
  MPI& operator=(MPI other) = delete;

  // Communicators
  MPI_Comm CommWorld() const {
    return MPI_COMM_WORLD;
  }
  MPI_Comm CommLocal() const {
    MPI_Comm result;
    #ifdef HAVE_MPI_3
      auto status = MPI_Comm_split_type(CommWorld(), MPI_COMM_TYPE_SHARED, GetRank(),
                                      MPI_INFO_NULL, &result);
      if (status != MPI_SUCCESS) { Error(status); }
    #else
      // Not implemented
      // see https://blogs.fau.de/wittmann/2013/02/mpi-node-local-rank-determination/
    #endif
    return result;
  }

  // Retrieves the MPI size and rank
  size_t GetSize() const {
    auto result = 0;
    auto status = MPI_Comm_size(CommWorld(), &result);
    if (status != MPI_SUCCESS) { Error(status); }
    return static_cast<size_t>(result);
  }
  size_t GetRank() const {
    auto result = 0;
    auto status = MPI_Comm_rank(CommWorld(), &result);
    if (status != MPI_SUCCESS) { Error(status); }
    return static_cast<size_t>(result);
  }

  // Retrieves the size and rank within a node (i.e. which rank within a node am I)
  size_t GetLocalCount() const {
    auto result = 0;
    auto status = MPI_Comm_size(CommLocal(), &result);
    if (status != MPI_SUCCESS) { Error(status); }
    return static_cast<size_t>(result);
  }
  size_t GetLocalRank() const {
    auto result = 0;
    auto status = MPI_Comm_rank(CommLocal(), &result);
    if (status != MPI_SUCCESS) { Error(status); }
    return static_cast<size_t>(result);
  }

  // Retrieves the total number of nodes and the node-rank (i.e. which node am I)
  size_t GetNodeCount() const {
    return GetSize()/GetLocalCount();
  }
  size_t GetNodeRank() const {
    return GetRank()/GetLocalCount();
  }

  // Buffer communication functions
  template <typename T>
  void Send(const size_t size, const size_t dest_rank, const size_t tag,
            T* src, const MPI_Comm comm) const {
    MPI_Send(src, size*sizeof(T), MPI_CHAR, dest_rank, tag, comm);
  }
  template <typename T>
  void Recv(const size_t size, const size_t src_rank, const size_t tag,
             T* dest, const MPI_Comm comm) const {
    MPI_Status state;
    MPI_Recv(dest, size*sizeof(T), MPI_CHAR, src_rank, tag, comm, &state);
  }
  template <typename T>
  void CopyFromTo(const size_t size, const size_t src_rank, const size_t dest_rank,
                  T* src, T* dest) const {
    if (GetRank() == src_rank) { Send(size, dest_rank, dest_rank, src); }
    if (GetRank() == dest_rank) { Recv(size, src_rank, dest_rank, dest); }
  }

  // Collectives
  int AllReduce(const int src) {
    int result = 0;
    MPI_Allreduce(&src, &result, 1, MPI_INT, MPI_SUM, CommWorld());
    return result;
  }

  // Other public functions
  void Barrier() const {
    auto status = MPI_Barrier(CommWorld());
    if (status != MPI_SUCCESS) { Error(status); }
  }

 private:

  // Error handling
  [[noreturn]] void Error(const std::string &message) const {
    throw std::runtime_error("Internal MPI error: "+message);
  }
  [[noreturn]] void Error(const int status) const {
    throw std::runtime_error("Internal MPI error with status: "+std::to_string(status));
  }
};

// =================================================================================================
} // namespace scbench

// SCBENCH_MPIPP11_H_
#endif
