
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This file provides the base-class for the SCBench benchmarks. All level 0 benchmarks inherit
// from this class. It provides the basic MPI and CUDA initialization, methods to time a given
// function, and methods to print information to screen and file.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#ifndef SCBENCH_SCBENCH_H_
#define SCBENCH_SCBENCH_H_

// The C++ interfaces to MPI and CUDA
#include "mpipp11.h"
#include "cupp11.h"

// The timers (header-only)
#include "timings.h"

#include <string>
#include <vector>
#include <functional>
#include <algorithm>
#include <map>

namespace scbench {
// =================================================================================================

// See comment at top of file for a description of the class
class SCBench {
 public:

  // Enables or disables GPU-direct
  #if GPUDIRECT
    static constexpr auto kUseGPUDirect = true;
  #else
    static constexpr auto kUseGPUDirect = false;
  #endif

  // Common list of arguments to the benchmarks
  struct Arguments {
    // MPI/node configuration
    size_t num_ranks = 1;     // Total number of MPI ranks
    size_t rank = 0;          // MPI rank ID (maximum value equal to the number of ranks - 1)
    size_t num_nodes = 1;     // Number of physical nodes
    size_t node_id = 0;       // MPI node ID (maximum value equal to the number of nodes - 1)
    size_t num_local = 1;     // Number of processes (==devices) within a node
    size_t local_id = 0;      // ID within a node, equal to the ID of the GPU in this node
    // Benchmark configuration
    size_t min = 0;           // Bandwidth/calculation tests starts from this value
    size_t max = 0;           // Bandwidth/calculation tests stop at this value
  };

  // The number of repetitions for measurements
  static constexpr size_t kRepetitionsOuter = 8;
  static constexpr size_t kRepetitionsInnerBandwidth = 1; // Watch out for caching
  static constexpr size_t kRepetitionsInnerCalculation = 2;
  static constexpr size_t kRepetitionsInnerLatency = 100;

  // Constants holding start and end strings for terminal-output in colour
  const std::string kPrintRed{"\x1b[31m"};
  const std::string kPrintGreen{"\x1b[32m"};
  const std::string kPrintPink{"\x1b[35m"};
  const std::string kPrintBold{"\x1b[1m"};
  const std::string kPrintEnd{"\x1b[0m"};

  // The public interface: this is what is visible to the outside world. Note that the derived
  // classes also have access to the protected methods.
  explicit SCBench(int argc, char *argv[]);
  void Run(const std::string &name);
  void PrintTable(const std::string &name) const;

  // ===============================================================================================
 private:

  // The virtual benchmark function, to be implemented by a derived class
  virtual void Benchmark() = 0;

  // Retrieves all arguments from the command-line. Currently, the command-line arguments are not
  // used, arguments are only set based on the MPI environment.
  Arguments GetArguments(int argc, char *argv[]);

  // ===============================================================================================
 protected:

  // Takes a function as an argument and executes it while measuring execution time. This includes
  // two MPI barries: to make sure the measured time is the time of the slowest MPI process. The
  // timing result will thus be more or less the same on each rank.
  using TimedFunction = std::function<void(const size_t size)>;
  Timings TimedExecution(const TimedFunction &timed_function, const size_t size,
                         const size_t repetitions_inner);

  // Prints the header of a new benchmark
  void StartBenchmark(const std::string &summary, const std::string &title);

  // Tests a given benchmark function in a loop over a size argument, ranging from a user-defined
  // minimum to a maximum in powers of 2.
  void TestBandwidth(const TimedFunction timed_function, const size_t modifier,
                     const std::string &id);
  void TestLatency(const TimedFunction timed_function, const std::string &id);
  void TestCalculation(const TimedFunction timed_function, const size_t modifier,
                       const std::string &id);

  // Reports the timing results to screen and returns the value of the printed metric
  float PrintBandwidth(const Timings &timings, const size_t bytes);
  float PrintLatency(const Timings &timings);
  float PrintCalculation(const Timings &timings, const size_t ops);

  // MPI members
  MPI mpi_;
  bool mpi_process_enabled_; // Some processes might be disabled for a particular benchmark
  size_t num_processes_; // This represents the number of processes still enabled

  // Benchmark settings
  Arguments args_;

  // CUDA device information
  Platform platform_;
  size_t device_id_;
  Device device_;
  Context context_;
  Queue queue_;
  Event event_;

  // Collection of results from the individual benchmarks
  using Collection = std::map<std::string,float>;
  Collection results_bandwidth_;
  Collection results_latency_;
  Collection results_calculation_;
};

// =================================================================================================
} // namespace scbench

// SCBENCH_SCBENCH_H_
#endif
