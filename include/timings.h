
// =================================================================================================
// This file is part of the SCBench project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This file implements the timings class. This allows timing data from multiple runs to be
// collected in a single vector. This data can then be analysed, e.g. to obtain the best-case time
// or the standard deviation.
// This is a header-only class.
//
// =================================================================================================
//
// Copyright 2015 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

#ifndef SCBENCH_TIMINGS_H_
#define SCBENCH_TIMINGS_H_

#include <string>
#include <vector>
#include <algorithm>

namespace scbench {
// =================================================================================================

// See comment at top of file for a description of the class
class Timings {
 public:

  // When printing the distribution to screen, this is the length of the visualisation
  static const auto kPrintLength = 10;

  // Creates a new empty vector of timings
  Timings(const size_t size): data_(size, 0.0f) { }

  // Getter and setter
  float operator[](const size_t i) const { return data_[i]; }
  void Add(const size_t i, const float value) { data_[i] = value; }

  // Retrieves the best timing, i.e. the one with the minimum value
  float GetBest() const {
    auto best_time = std::min_element(data_.begin(), data_.end());
    return *best_time;
  }

  // Creates a visualisation of the distribution of the timings. Crosses one the left represent
  // fast cases, while crosses on the right represent slow cases. If there is only one cross
  // visible, then there is no or limited variation. The more crosses on the right, the more
  // variation and the higher the standard deviation.
  std::string GetDistribution() const {
    auto counters = std::vector<size_t>(kPrintLength, 0);
    for (auto &val: data_) {
      auto fraction = GetBest() / val;
      auto index = kPrintLength - (fraction * kPrintLength) / 1;
      if (index == 0) { counters[0]++; }
      else { counters[index-1]++; }
    }
    auto result = std::string{};
    for (auto &counter: counters) {
      result += (counter == 0) ? "-" : "x";
    }
    return result;
  }

  // The private storage
 private:
  std::vector<float> data_;
};

// =================================================================================================
} // namespace scbench

// SCBENCH_TIMINGS_H_
#endif
