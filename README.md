 
SCBench: A CUDA/MPI benchmark suite for supercomputers
================

SCBench is a benchmark suite containing micro-benchmarks and small synthetic applications. In contrast to existing MPI benchmark suites, SCBench is targeted at supercomputers with CUDA-based accelerators. It contains the following benchmarks:

* Level 0 micro-benchmarks:
    - __comm_host__: CPU memory bandwidth and latency
    - __comm_device__: GPU memory and CPU-GPU bandwidth and latency
    - __comm_mpi__: inter/intra node CPU/GPU communication
    - __calc_device__: GPU compute throughput
    - __calc_host__: CPU compute throughput
* Level 1 synthetic applications:
    - __jacobi__: Jacobi solver (with GPU-Direct)

All benchmarks are MPI-enabled. Some will report aggregate metrics over all MPI-ranks, others will only perform work for specific ranks. Details of the benchmarks are given below.

SCBench uses the Claduc CUDA C++11 header, which can be easily interchanged for its OpenCL counter-part. However, the GPU compute kernels are currently CUDA-only, as is the GPU-Direct technology.


Compilation instructions
-------------

The pre-requisites are:

* CMake 2.8.10 or higher
* A C++11 compiler (e.g. GCC 4.7 or newer)
* CUDA 7.0 or higher
* An MPI-3 library

Use CMake to create an out-of-source build:

```bash
mkdir build
cd build
cmake ..
make
```


Benchmark details
-------------

__Level 0 comm_host__:

Tests the host's (=CPU) memory bandwidth and latency in multiple ways:
* Regular `std::vector` copied using `std::copy_n`.
* Regular `std::vector` copied using `memcpy`.
* CUDA pinned host memory copied using `memcpy`.
This benchmark can run with any number of MPI processes, results are aggregated.

__Level 0 comm_device__:

Tests the bandwidth and latency of the device (=GPU) memory and the PCI-Express bus in multiple ways:
* Regular `std::vector` and a CUDA host-to-device copy.
* CUDA pinned host memory and a CUDA host-to-device copy.
* Regular `std::vector` and a CUDA device-to-host copy.
* CUDA pinned host memory and a CUDA device-to-host copy.
* CUDA device-to-device copy.

This benchmark automatically limits the number of active MPI processes to the number of CUDA devices found. The results are aggregated over all CUDA devices.

__Level 0 comm_mpi__:

Tests the MPI bandwidth and latency. Two types of tests are considered:
* Intra-node tests, ran with 2 processes on the same node.
* Inter-node tests, ran with 2 processes on a different node.

In both cases, three tests are performed:
- Host-to-host MPI copy.
- Device-to-device copy through MPI and separate CUDA host-to-device and device-to-host copies.
- Device-to-device copy through GPU-Direct, optionally using RDMA if supported.

__Level 0 calc_device__:

Tests arithmetic instruction throughput on the device (=GPU). This uses a mix of CUDA assembly and regular CUDA C++. Note that these benchmarks do not necessarily get to the peak performance of the device. The tests are:
* 32-bit integer addition.
* 32-bit integer multiplication.
* 32-bit floating point multiplication.
* 32-bit floating point fused multiply-add.
* 64-bit floating point fused multiply-add.
* 32-bit floating point cosine fast-math.

This benchmark automatically limits the number of active MPI processes to the number of CUDA devices found. The results are aggregated over all CUDA devices.

__Level 0 calc_host__:

Tests arithmetic instruction throughput on the host (=CPU). This is an early version of the benchmark: inspection of assembly shows that the micro-benchmark is not perfect. It currently tests compiler-vectorized versions of:
* 32-bit integer multiplication.
* 32-bit floating point multiplication.
* 32-bit fused multiply-add.

This benchmark can run with any number of MPI processes, results are aggregated.

__Level 1 jacobi__:

This is an example of a synthetic benchmark, a Jacobi solver taken from the [NVIDIA parallel for-all blog](http://devblogs.nvidia.com/parallelforall/benchmarking-cuda-aware-mpi/). The source code is taken directly from [GitHub](https://github.com/parallel-forall/code-samples/tree/master/posts/cuda-aware-mpi-example/src) and is stored in `src/level1/jacobi/source`. The file `src/level1/jacobi/scbench.cc` adjusts the benchmark to fit into the SCBench environment: 1) command-line arguments are no longer required, and 2) the performance output is written to a CSV-file.

The performance of the solver is best when launching a number of MPI processes equal to the number of CUDA devices. The use of GPU-Direct can be toggled by compiling either `cuda_aware_mpi.cc` or `cuda_normal_mpi.cc`.


Graphs: 
-------------

An R-script is included (`graphs/graphs.r`) to automatically generate performance graphs. It has to be executed in the folder in which one or more resulting CSV files are stored.

